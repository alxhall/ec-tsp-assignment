import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

class Chromosome {

	private static final Random RANDOM = new Random();

	/**
	 * The list of cities, which are the genes of this chromosome.
	 */
	protected int[] cityList;

	/**
	 * The cost of following the cityList order of this chromosome.
	 */
	protected double cost;

	/**
	 * @param cities
	 *            The order that this chromosome would visit the cities.
	 */
	Chromosome(City[] cities) {
		this(cities.length);
	}

	/** Construct a random order of cities of the given length */
	Chromosome(int length) {
		cityList = new int[length];
		List<Integer> perm = new ArrayList<>();
		for (int i = 0; i < length; i++) {
			perm.add(i);
		}
		Collections.shuffle(perm);
		for (int i = 0; i < length; i++) {
			cityList[i] = perm.get(i);
		}
	}

	/** Copy constructor */
	Chromosome(Chromosome c) {
		this.cost = c.cost;
		this.cityList = Arrays.copyOf(c.cityList, c.cityList.length);
	}

	/**
	 * Combines two parent chromosomes using the edge recombination operator.
	 * Based on http://en.wikipedia.org/wiki/Edge_recombination_operator
	 */
	Chromosome(Chromosome parent1, Chromosome parent2) {

		// Create the union adjacency list
		int length = parent1.cityList.length;
		List<HashSet<Integer>> adjacency = new ArrayList<>(length);
		for (int i = 0; i < length; i++) {
			adjacency.add(new HashSet<Integer>());
		}

		Chromosome[] parents = { parent1, parent2 };
		for (int i = 0; i < length - 1; i++) {
			for (Chromosome parent : parents) {
				int current = parent.getCity(i);
				int next = parent.getCity(i + 1);
				adjacency.get(current).add(next);
				adjacency.get(next).add(current);
			}
		}

		cityList = new int[length]; // K
		Set<Integer> K = new HashSet<>(); // Same but efficient lookup

		// Let N be the first node of a random parent
		int N = (RANDOM.nextBoolean() ? parent1 : parent2).getCity(0);

		// While Length(K) < Length(Parent):
		for (int i = 0; i < length - 1; i++) {

			// K := K, N (append N to K)
			cityList[i] = N;
			K.add(N);

			// Remove N from all neighbor lists
			for (int neighbor : adjacency.get(N)) {
				adjacency.get(neighbor).remove(N);
			}

			// If N's neighbor list is non-empty
			if (!adjacency.get(N).isEmpty()) {

				// then let N* be the neighbor of N with the fewest neighbors in
				// its list (or a random one, should there be multiple)
				List<Integer> minimalNeighbours = new ArrayList<>();
				int min = Integer.MAX_VALUE;
				for (int neighbor : adjacency.get(N)) {
					int numNeighbours = adjacency.get(neighbor).size();
					if (numNeighbours < min) {
						min = numNeighbours;
						minimalNeighbours.clear();
						minimalNeighbours.add(neighbor);
					} else if (numNeighbours == min) {
						minimalNeighbours.add(neighbor);
					}
				}
				N = minimalNeighbours.get(RANDOM.nextInt(minimalNeighbours
						.size())); // N := N*
			} else {

				// else let N* be a randomly chosen node that is not in K
				while (K.contains(N)) {
					N = RANDOM.nextInt(length); // N := N*
				}
			}

		}
		cityList[length - 1] = N;
	}

	/**
	 * Calculate the cost of the specified list of cities.
	 * 
	 * @param cities
	 *            A list of cities.
	 */
	void calculateCost(City[] cities) {
		cost = 0;
		for (int i = 0; i < cityList.length - 1; i++) {
			double dist = cities[cityList[i]]
					.proximity(cities[cityList[i + 1]]);
			cost += dist;
		}
	}

	/**
	 * Get the cost for this chromosome. This is the amount of distance that
	 * must be traveled.
	 */
	double getCost() {
		return cost;
	}

	/**
	 * @param i
	 *            The city you want.
	 * @return The ith city.
	 */
	int getCity(int i) {
		return cityList[i];
	}

	/**
	 * Set the order of cities that this chromosome would visit.
	 * 
	 * @param list
	 *            A list of cities.
	 */
	void setCities(int[] list) {
		for (int i = 0; i < cityList.length; i++) {
			cityList[i] = list[i];
		}
	}

	/**
	 * Set the index'th city in the city list.
	 * 
	 * @param index
	 *            The city index to change
	 * @param value
	 *            The city number to place into the index.
	 */
	void setCity(int index, int value) {
		cityList[index] = value;
	}

	/**
	 * "Cuts out a segment of a tour and re-inserts it in opposite direction. Changes two edges in a tour"
	 */
	void invert() {

		// Select two nodes representing the subsequence to invert.
		// Ensure that we don't just invert the entire list but we do invert
		// something
		int node1, node2, start, end;
		int length = cityList.length;
		do {
			node1 = RANDOM.nextInt(length);
			node2 = RANDOM.nextInt(length);
			start = Math.min(node1, node2);
			end = Math.max(node1, node2);
		} while (start == end || start == 0 && end == length - 1);

		// Invert the subsequence by swapping pairs from the start to halfway
		int pos1, pos2;
		int half = (int) Math.ceil((end - start) / 2.0);
		for (int i = 0; i < half; i++) {
			pos1 = start + i;
			pos2 = end - i;
			swap(pos1, pos2);
		}

	}

	/**
	 * "Selects a random position in the genotype, then inserts this gene into a new (random) position. Changes three edges in a tour."
	 */
	void translocate() {
		int start, end;
		int length = cityList.length;
		do {
			start = RANDOM.nextInt(length);
			end = RANDOM.nextInt(length);
		} while (start == end);
		int step = end > start ? 1 : -1;
		for (int i = start; i != end; i += step) {
			swap(i, i + step);
		}
	}

	/**
	 * "Exchanges two randomly chosen points in a tour. Changes up to four edges per mutation."
	 */
	void transpose() {
		int node1, node2;
		do {
			node1 = RANDOM.nextInt(cityList.length);
			node2 = RANDOM.nextInt(cityList.length);
		} while (node1 == node2);
		swap(node1, node2);
	}

	/**
	 * "Shifts randomly chosen segment (between 2 points) to a third selected point. Changes up to six edges per mutation."
	 */
	void shift() {

		// Select two nodes representing the subsequence to shift.
		// Ensure that we don't select the entire list but we do select
		// something
		int node1, node2, start, end;
		int length = cityList.length;
		do {
			node1 = RANDOM.nextInt(length);
			node2 = RANDOM.nextInt(length);
			start = Math.min(node1, node2);
			end = Math.max(node1, node2);
		} while (start == end || start == 0 && end == length - 1);

		// Select where the subsequence will move to
		int newStart;
		int subLength = end - start + 1;
		int maxStart = length - subLength + 1;
		do {
			newStart = RANDOM.nextInt(maxStart);
		} while (newStart == start);

		// Copy the subsequence into a temporary array
		int[] sub = new int[subLength];
		for (int i = 0; i < subLength; i++) {
			sub[i] = cityList[i + start];
		}

		// Move the displaced nodes. This is more messy, so we use a list
		List<Integer> temp = new ArrayList<>();
		int newEnd = newStart + subLength - 1;
		int toStart, fromStart, fromEnd;
		if (newStart > start) {
			toStart = start;
			fromStart = end + 1;
			fromEnd = newEnd;
		} else {
			toStart = newEnd + 1;
			fromStart = newStart;
			fromEnd = start - 1;
		}
		for (int from = fromStart; from <= fromEnd; from++) {
			temp.add(cityList[from]);
		}
		for (int i = 0; i < temp.size(); i++) {
			cityList[toStart + i] = temp.get(i);
		}

		// Copy the shifting subsequence back
		for (int i = 0; i < subLength; i++) {
			cityList[i + newStart] = sub[i];
		}

		Set<Integer> set = new HashSet<>(cityList.length);
		for (int c : cityList) {
			set.add(c);
		}
		if (set.size() != length) {
			System.out.println("HELLO");
		}
	}

	/** Swaps the items in cityList at the given positions */
	private void swap(int pos1, int pos2) {
		int temp = cityList[pos1];
		cityList[pos1] = cityList[pos2];
		cityList[pos2] = temp;
	}

	/**
	 * Sort the chromosomes by their cost.
	 * 
	 * @param chromosomes
	 *            An array of chromosomes to sort.
	 * @param num
	 *            How much of the chromosome list to sort.
	 */
	public static void sortChromosomes(Chromosome chromosomes[], int num) {
		Chromosome ctemp;
		boolean swapped = true;
		while (swapped) {
			swapped = false;
			for (int i = 0; i < num - 1; i++) {
				if (chromosomes[i].getCost() > chromosomes[i + 1].getCost()) {
					ctemp = chromosomes[i];
					chromosomes[i] = chromosomes[i + 1];
					chromosomes[i + 1] = ctemp;
					swapped = true;
				}
			}
		}
	}

}
